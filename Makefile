#
#=====================================================================================
#
#   	Project:  Makefile for the Latex talk given at a IEEE SB meeting in Klagenfurt
#
#       Version:  1.0
#       Created:  28/05/2013 22:12:00
#      Revision:  none
#
#        Author:  Alessandro Crismani (AC), alessandro.crismani@ieee.org
#       Company:  Aplen-Adria Universitaet Klagenfurt
#
#=====================================================================================
#
# SHELL
$SHELL=/bin/bash

# MAKE program
MAKE		 := make
MAKECLEAN    := make clean
#
# Latex binaries and options
LATEX        := pdflatex
LATEXFLGS    := -interaction=nonstopmode --shell-escape

# Included directories and libraries
SOURCEDIR    := Sources
FIGDIR       := Figures

# Project targets
SLIDES       := LatexTalk.pdf
TEXMAIN      := LatexTalk.tex
TARGETS      := $(SLIDES)
SNIPPETS	 := $(wildcard Sources/*.tex)

include Colors.mk

.PHONY: default clean

default: $(TARGETS)

clean:
	@printf '$(RED)Remove the pdf ...$(NC)\n'
	@rm -f $(SLIDES)
	@printf '$(RED)... done$(NC)\n'
	@printf '\n'

$(SLIDES): $(TEXMAIN) $(SNIPPETS)
	@printf '$(YELLOW)Compile slides ...$(NC)\n'
	@$(LATEX) $(LATEXFLGS) $(TEXMAIN) > /dev/null 2>&1
	@printf '$(YELLOW)... slides are ready$(NC)\n'
	@printf 'Removing rubbish ...\n'
	@rm *.log *.aux *.toc *.nav *.pyg *.snm *.vrb *.out
	@printf '\n'
