# Colors for Makefiles
gray	= \033[1;30m
red		= \033[0;31m
RED		= \033[1;31m
blue	= \033[0;34m
BLUE	= \033[1;34m
green	= \033[0;32m
GREEN	= \033[1;32m
cyan	= \033[0;36m
CYAN	= \033[1;36m
white	= \033[0;37m
WHITE	= \033[1;37m
yellow	= \033[0;33m
YELLOW	= \033[1;33m
NC		= \033[0m # No Color
