\documentclass[14pt]{beamer}

% Font used for the presentation
\usepackage[default]{cantarell}
% Vertically align tables
\usepackage{array}
% Tikz for plotting and used libraries
\usepackage{tikz}
\usetikzlibrary{shapes,arrows}
\usetikzlibrary{plotmarks}
\usetikzlibrary{calc,intersections,through,backgrounds}
% PGFplots for including plots of data
\usepackage{pgfplots}
\pgfplotsset{compat=1.5} % Labels closer to axis
% Color table rows and columns
\usepackage{color, colortbl}
\newcolumntype{R}{>{\columncolor{red!50}}l}
% Include LaTeX source code and color it
\usepackage{fancyvrb}
\usepackage{minted}
\usemintedstyle{perldoc}
% Include figures in the 'Figures' folder
\usepackage{graphicx} \DeclareGraphicsExtensions{.jpg} \graphicspath{{Figures/}}
\usepackage{graphicx} \DeclareGraphicsExtensions{.png} \graphicspath{{Figures/}}
\usepackage{graphicx} \DeclareGraphicsExtensions{.pdf} \graphicspath{{Figures/}}
% Labels of Tables and Figures do not contain 'Table' or 'Figure'
\usepackage[labelformat=empty]{caption}
% URLs and Refs
\usepackage{hyperref}

% Slide are compiled in presentation mode
\mode<presentation>{}

%------------------------------------------------------------
% Beamer theme customisation
%------------------------------------------------------------

% Choose theme
\usetheme{default}
\definecolor{fggray}{HTML}{4d4d4d}
\definecolor{bggray}{HTML}{dedede}
\setbeamercolor{framecol}{fg=fggray,bg=bggray}

% Choose innertheme
\setbeamercolor{itemize item}{fg=fggray}
\setbeamercolor{itemize subitem}{fg=fggray}

% Font, Style and center title
\setbeamertemplate{frametitle}[default][]
\setbeamerfont{frametitle}{series=\sc,size=\small}
\setbeamercolor*{titlelike}{fg=fggray,bg=bggray}
\setbeamertemplate{headline}{\vspace{0.20cm}}

% Remove navigation symbols from footline
\setbeamertemplate{navigation symbols}{}

% Color of background
\setbeamercolor{normal text}{bg=white,fg=fggray}

% Change space between itemize
\setlength{\parskip}{10pt}
\setlength{\parsep}{10pt}
\setlength{\itemsep}{10pt}
\makeatletter
\def\@listi{\leftmargin\leftmargini \parskip=0pt \parsep=0pt \topsep=8pt \itemsep=5pt}
\def\@listii{\leftmargin\leftmargini \parskip=0pt \parsep=0pt \topsep=5pt \itemsep=3pt}
\def\@listiii{\leftmargin\leftmargini \parskip=0pt \parsep=0pt \topsep=5pt \itemsep=3pt}
   \let\@listI\@listi
\makeatother

% Write TikZ as it should be written
\newcommand{\TikZ}{Ti\textit{k}Z}

\begin{document}
\title{Ingegneria dell'informazione - Lezione di Trasmissione Numerica}
\author{Alessandro Crismani}
\date{\today}

\begin{frame}
    \begin{center}
        \includegraphics[width=0.6\textwidth]{sb-logo.pdf} \\ [0.5cm]
        ~\hfill\parskip0pt\parsep0pt\itemsep0pt\begin{beamercolorbox}[shadow=false,rounded=true,wd=0.8\textwidth,center]{framecol}
                {\large Writing beautiful documents} \\ [0.3cm]
                {\textit{\large with}} \\ [0.3cm] {\huge \LaTeX} \\
                {Alessandro Crismani} \\
                {\scriptsize 6$^\text{th}$ June 2013} \\
        \end{beamercolorbox}\hfill~
    \end{center}
\end{frame}

\begin{frame}
    %\frametitle{\ldots}
    \vspace*{-0.2cm}
    \begin{center}
    ~\hfill\parskip0pt\parsep0pt\itemsep0pt\begin{beamercolorbox}[shadow=false,rounded=true,wd=0.95\textwidth,center]{framecol}
        \includegraphics[width=0.8\textwidth]{write-a-paper.jpg}
    \end{beamercolorbox}\hfill~
    \end{center}
\end{frame}

\begin{frame}
    \frametitle{So you think Word and Matlab should suffice\ldots}
    ~\hfill\parskip0pt\parsep0pt\itemsep0pt\begin{beamercolorbox}[shadow=false,rounded=true,wd=0.95\textwidth,center]{framecol}
            \begin{tikzpicture}
                \node (img1) {\includegraphics[width=5cm]{word-sucks-one.jpg}};
                \pause
                \node (img2) at (img1.south east) [xshift=-0.5cm] [yshift=-1cm] {\includegraphics[width=4cm]{word-sucks-two.jpg}};
                \pause
                \node (img3) at (img1.south west) [xshift=-0.5cm] [yshift=-1cm] {\includegraphics[width=5.5cm]{word-sucks-three.jpg}};
                \pause
                \node (img4) at (img1.south) [yshift=-3cm] {\includegraphics[width=7cm]{word-sucks-four.jpg}};
            \end{tikzpicture}
    \end{beamercolorbox}\hfill~
\end{frame}

\begin{frame}
    \frametitle{\ldots}
    \includegraphics[width=0.7\textwidth]{matlab-plot-sucks.png}
\end{frame}

\begin{frame}
    \frametitle{\ldots}
    \vspace*{-0.0cm}\includegraphics[width=0.9\textwidth]{matlab-wat.png}
\end{frame}

\begin{frame}
    \frametitle{\ldots They Don't!}
    ~\hfill\parskip0pt\parsep0pt\itemsep0pt\begin{beamercolorbox}[shadow=false,rounded=true,wd=0.9\textwidth,center]{framecol}
            \begin{tikzpicture}
                \node (img1) {\includegraphics[height=4cm]{fail-one.jpg}};
                \pause
                \node (img2) at (img1.south east) {\includegraphics[height=4cm]{fail-two.jpg}};
                \pause
                \node (img3) at (img2.south west) [yshift=1cm] {\includegraphics[height=4cm]{fail-three.jpg}};
                \pause
                \node (img4) at (img3.west) [yshift=1cm] {\includegraphics[height=4cm]{facepalm.jpg}};
            \end{tikzpicture}
    \end{beamercolorbox}\hfill~
\end{frame}

\begin{frame}
    \frametitle{And then the reviewer says \ldots}
    \pause
    \begin{center}
        \includegraphics[width=0.8\textwidth]{shall-not-pass.jpg}
    \end{center}
\end{frame}

\begin{frame}
    \frametitle{Can we make it look better?}
    \textcolor{red}{\Large YES}
    \begin{itemize}
        \item \LaTeX
        \item \TikZ
        \item PGF
        \item Gnuplot $\rightarrow$ \TikZ
        \item matplotlib $\rightarrow$ \TikZ
    \end{itemize}
\end{frame}

\begin{frame}
    \frametitle{Is it easier than Word or Matlab or ``you name it''?}
    \textcolor{red}{\Large Probably NOT}

    No easy cake, however:
    \pause
    \begin{center}
        \vspace{-0.5cm}
        \includegraphics[width=0.6\textwidth]{stronger.jpg}
    \end{center}
\end{frame}

\begin{frame}[fragile]
    \frametitle{A simple \LaTeX document}
    \inputminted[fontsize=\scriptsize, style=colorful, frame=single]{latex}{Sources/simple-latex.tex}
\end{frame}

\begin{frame}[fragile]
    \frametitle{Math using \LaTeX}
    Math looks awesome, and it is easy too!

    \vspace{0.3cm}
    \inputminted[fontsize=\scriptsize, style=colorful, frame=single]{latex}{Sources/math-one.tex}
    \vspace{-0.5cm}
    {\small \input{Sources/math-one.tex}}

    \pause
    \vspace{0.3cm}
    \inputminted[fontsize=\scriptsize, style=colorful, frame=single]{latex}{Sources/math-two.tex}
    \vspace{-0.5cm}
    {\small \input{Sources/math-two.tex}}

    \pause
    \vspace{0.3cm}
    \inputminted[fontsize=\scriptsize, style=colorful, frame=single]{latex}{Sources/math-three.tex}
    \vspace{-0.5cm}
    {\small \input{Sources/math-three.tex}}
\end{frame}

\begin{frame}[fragile]
    \frametitle{Math using \LaTeX - some more}
    Align equations, piecewise functions? Check!
    \vspace{-0.5cm}
    \inputminted[fontsize=\scriptsize, style=colorful, frame=single]{latex}{Sources/math-four.tex}
    \vspace{-0.5cm}
    {\small \input{Sources/math-four.tex}}

    \pause
    \vspace{-0.4cm}
    \inputminted[fontsize=\scriptsize, style=colorful, frame=single]{latex}{Sources/math-five.tex}
    \vspace{-0.8cm}
    {\small \input{Sources/math-five.tex}}
\end{frame}

\begin{frame}
    \frametitle{Plotting with \LaTeX (using \TikZ)}
    Wow, math looks good, but \ldots \\
    \ldots can I include it in graphics?

    \pause
    \textcolor{red}{\Large HELL YES}
    \vspace{0.5cm}

    \pause
    \begin{columns}
        \begin{column}{0.48\textwidth}
            \includegraphics[width=5.5cm]{mrc-matlab.pdf}
        \end{column}
        \begin{column}{0.48\textwidth}
            \input{Sources/mrc-tikz.tex}
        \end{column}
    \end{columns}
\end{frame}

\begin{frame}[fragile]
    \frametitle{Plotting with \LaTeX (using \TikZ) - step by step example}
    Prerequisite
    \begin{itemize}
        \item Data stored in a text file, column-wise
    \end{itemize}
    \begin{overprint}
    \only<1>{\begin{columns}
        \begin{column}{0.35\textwidth}
            \vspace{-1cm}
            \begin{itemize}
                \item[] figure();
                \item[] hold on;
                \item[] plot(\textcolor{red}{x},y);
                \item[] plot(\textcolor{red}{x},z);
            \end{itemize}
        \end{column}
        \begin{column}{0.65\textwidth}
            \begin{table}
                \begin{tabular}{Rll}
                    5	& 5.81	& 0.57 \\ \noalign{\vspace*{-1pt}}
                    10	& 4.72	& 0.58 \\ \noalign{\vspace*{-1pt}}
                    15	& 4.57	& 0.59 \\ \noalign{\vspace*{-1pt}}
                    20	& 4.42	& 0.59 \\ \noalign{\vspace*{-1pt}}
                    25	& 4.11	& 0.61 \\ \noalign{\vspace*{-1pt}}
                    30	& 3.99	& 0.63 \\ \noalign{\vspace*{-1pt}}
                    35	& 3.73	& 0.64 \\ \noalign{\vspace*{-1pt}}
                    40	& 3.78	& 0.63
                \end{tabular}
                \caption{Content of data.txt}
            \end{table}
        \end{column}
    \end{columns}
    }
    \only<2>{\begin{columns}
        \begin{column}{0.35\textwidth}
            \vspace{-1cm}
            \begin{itemize}
                \item[] figure();
                \item[] hold on;
                \item[] plot(x,\textcolor{red}{y});
                \item[] plot(x,z);
            \end{itemize}
        \end{column}
        \begin{column}{0.65\textwidth}
            \begin{table}
                \begin{tabular}{lRl}
                    5	& 5.81	& 0.57 \\ \noalign{\vspace*{-1pt}}
                    10	& 4.72	& 0.58 \\ \noalign{\vspace*{-1pt}}
                    15	& 4.57	& 0.59 \\ \noalign{\vspace*{-1pt}}
                    20	& 4.42	& 0.59 \\ \noalign{\vspace*{-1pt}}
                    25	& 4.11	& 0.61 \\ \noalign{\vspace*{-1pt}}
                    30	& 3.99	& 0.63 \\ \noalign{\vspace*{-1pt}}
                    35	& 3.73	& 0.64 \\ \noalign{\vspace*{-1pt}}
                    40	& 3.78	& 0.63
                \end{tabular}
                \caption{Content of data.txt}
            \end{table}
        \end{column}
    \end{columns}
    }
    \only<3>{\begin{columns}
        \begin{column}{0.35\textwidth}
            \vspace{-1cm}
            \begin{itemize}
                \item[] figure();
                \item[] hold on;
                \item[] plot(x,y);
                \item[] plot(x,\textcolor{red}{z});
            \end{itemize}
        \end{column}
        \begin{column}{0.65\textwidth}
            \begin{table}
                \begin{tabular}{llR}
                    5	& 5.81	& 0.57 \\ \noalign{\vspace*{-1pt}}
                    10	& 4.72	& 0.58 \\ \noalign{\vspace*{-1pt}}
                    15	& 4.57	& 0.59 \\ \noalign{\vspace*{-1pt}}
                    20	& 4.42	& 0.59 \\ \noalign{\vspace*{-1pt}}
                    25	& 4.11	& 0.61 \\ \noalign{\vspace*{-1pt}}
                    30	& 3.99	& 0.63 \\ \noalign{\vspace*{-1pt}}
                    35	& 3.73	& 0.64 \\ \noalign{\vspace*{-1pt}}
                    40	& 3.78	& 0.63
                \end{tabular}
                \caption{Content of data.txt}
            \end{table}
        \end{column}
    \end{columns}
    }
    \end{overprint}
\end{frame}

\begin{frame}[fragile]
    \frametitle{Plotting with \LaTeX (using \TikZ) - step by step example}
    \begin{overprint}
    \only<1>{\begin{columns}[c]
        \begin{column}{0.6\textwidth}
            \hspace*{-1cm}\inputminted[fontsize=\scriptsize, frame=single]{latex}{Sources/plot-one.tex}
        \end{column}
        \begin{column}{0.4\textwidth}
            \hspace*{-0.75cm}\input{Sources/plot-one.tex}
        \end{column}
    \end{columns}}
    \only<2>{\begin{columns}[c]
        \begin{column}{0.6\textwidth}
            \hspace*{-1cm}\inputminted[fontsize=\scriptsize, frame=single]{latex}{Sources/plot-two.tex}
        \end{column}
        \begin{column}{0.4\textwidth}
            \hspace*{-0.75cm}\input{Sources/plot-two.tex}
        \end{column}
    \end{columns}}
    \only<3>{\begin{columns}[c]
        \begin{column}{0.6\textwidth}
            \hspace*{-1cm}\inputminted[fontsize=\scriptsize, frame=single]{latex}{Sources/plot-three.tex}
        \end{column}
        \begin{column}{0.4\textwidth}
            \hspace*{-0.75cm}\input{Sources/plot-three.tex}
        \end{column}
    \end{columns}}
    \only<4>{\begin{columns}[c]
        \begin{column}{0.6\textwidth}
            \hspace*{-1cm}\inputminted[fontsize=\scriptsize, frame=single]{latex}{Sources/plot-four.tex}
        \end{column}
        \begin{column}{0.4\textwidth}
            \hspace*{-0.75cm}\input{Sources/plot-four.tex}
        \end{column}
    \end{columns}}
    \only<5>{\begin{columns}[c]
        \begin{column}{0.6\textwidth}
            \hspace*{-1cm}\inputminted[fontsize=\scriptsize, frame=single]{latex}{Sources/plot-five.tex}
        \end{column}
        \begin{column}{0.4\textwidth}
            \hspace*{-0.75cm}\input{Sources/plot-five.tex}
        \end{column}
    \end{columns}}
    \only<6>{\begin{columns}[c]
        \begin{column}{0.6\textwidth}
            \hspace*{-1cm}\inputminted[fontsize=\scriptsize, frame=single]{latex}{Sources/plot-six.tex}
        \end{column}
        \begin{column}{0.4\textwidth}
            \hspace*{-0.75cm}\input{Sources/plot-six.tex}
        \end{column}
    \end{columns}}
    \only<7>{\begin{columns}[c]
        \begin{column}{0.6\textwidth}
            \hspace*{-1cm}\inputminted[fontsize=\scriptsize, frame=single]{latex}{Sources/plot-seven.tex}
        \end{column}
        \begin{column}{0.4\textwidth}
            \hspace*{-0.75cm}\input{Sources/plot-seven.tex}
        \end{column}
    \end{columns}}
    \end{overprint}
\end{frame}

\begin{frame}[fragile]
    \frametitle{Plotting with \LaTeX (using \TikZ) - 3D}
    \begin{overprint}
    \only<1>{\begin{columns}[c]
        \begin{column}{0.6\textwidth}
            \hspace*{-1cm}\inputminted[fontsize=\scriptsize, frame=single]{latex}{Sources/3D.tex}
        \end{column}
        \begin{column}{0.4\textwidth}
            \hspace*{-0.75cm}\input{Sources/3D.tex}
        \end{column}
    \end{columns}}
    \end{overprint}
\end{frame}

\begin{frame}[fragile]
    \frametitle{Plotting with \LaTeX (using \TikZ) - flow chart}
    \begin{overprint}
    \only<1>{\begin{columns}[c]
        \begin{column}{0.62\textwidth}
            \hspace*{-1cm}\inputminted[fontsize=\scriptsize, frame=single]{latex}{Sources/flow-one.tex}
        \end{column}
        \begin{column}{0.38\textwidth}
            \vspace{0.5cm}
            \hspace*{-0.25cm}\input{Sources/flow-one.tex}
        \end{column}
    \end{columns}}
    \only<2>{\begin{columns}[c]
        \begin{column}{0.62\textwidth}
            \hspace*{-1cm}\inputminted[fontsize=\scriptsize, frame=single]{latex}{Sources/flow-two.tex}
        \end{column}
        \begin{column}{0.38\textwidth}
            \vspace{0.5cm}
            \hspace*{-0.25cm}\input{Sources/flow-two.tex}
        \end{column}
    \end{columns}}
    \only<3>{\begin{columns}[c]
        \begin{column}{0.62\textwidth}
            \hspace*{-1cm}\inputminted[fontsize=\scriptsize, frame=single]{latex}{Sources/flow-three.tex}
        \end{column}
        \begin{column}{0.38\textwidth}
            \vspace{0.5cm}
            \hspace*{-0.25cm}\input{Sources/flow-three.tex}
        \end{column}
    \end{columns}}
    \only<4>{\begin{columns}[c]
        \begin{column}{0.62\textwidth}
            \hspace*{-1cm}\inputminted[fontsize=\scriptsize, frame=single]{latex}{Sources/flow-four.tex}
        \end{column}
        \begin{column}{0.38\textwidth}
            \vspace{0.5cm}
            \hspace*{-0.25cm}\input{Sources/flow-four.tex}
        \end{column}
    \end{columns}}
    \only<5>{\begin{columns}[c]
        \begin{column}{0.62\textwidth}
            \hspace*{-1cm}\inputminted[fontsize=\scriptsize, frame=single]{latex}{Sources/flow-five.tex}
        \end{column}
        \begin{column}{0.38\textwidth}
            \vspace{0.5cm}
            \hspace*{-0.25cm}\input{Sources/flow-five.tex}
        \end{column}
    \end{columns}}
    \end{overprint}
\end{frame}

\begin{frame}[fragile]
    \frametitle{Plotting with \LaTeX (using \TikZ) - TCP frame exchange}
    \begin{overprint}
    \only<1>{\begin{columns}[c]
        \begin{column}{0.64\textwidth}
            \hspace*{-1cm}\inputminted[fontsize=\scriptsize, frame=single]{latex}{Sources/syn-ack-one.tex}
        \end{column}
        \begin{column}{0.36\textwidth}
            \vspace{0.5cm}
            \hspace*{-0.25cm}\input{Sources/syn-ack-one.tex}
        \end{column}
    \end{columns}}
    \only<2>{\begin{columns}[c]
        \begin{column}{0.64\textwidth}
            \hspace*{-1cm}\inputminted[fontsize=\scriptsize, frame=single]{latex}{Sources/syn-ack-two.tex}
        \end{column}
        \begin{column}{0.36\textwidth}
            \vspace{0.5cm}
            \hspace*{-0.25cm}\input{Sources/syn-ack-two.tex}
        \end{column}
    \end{columns}}
    \only<3>{\begin{columns}[c]
        \begin{column}{0.64\textwidth}
            \hspace*{-1cm}\inputminted[fontsize=\scriptsize, frame=single]{latex}{Sources/syn-ack-three.tex}
        \end{column}
        \begin{column}{0.36\textwidth}
            \vspace{0.5cm}
            \hspace*{-0.25cm}\input{Sources/syn-ack-three.tex}
        \end{column}
    \end{columns}}
    \only<4>{\begin{columns}[c]
        \begin{column}{0.64\textwidth}
            \hspace*{-1cm}\inputminted[fontsize=\scriptsize, frame=single]{latex}{Sources/syn-ack-four.tex}
        \end{column}
        \begin{column}{0.36\textwidth}
            \vspace{0.5cm}
            \hspace*{-0.25cm}\input{Sources/syn-ack-four.tex}
        \end{column}
    \end{columns}}
    \only<5>{\begin{columns}[c]
        \begin{column}{0.64\textwidth}
            \hspace*{-1cm}\inputminted[fontsize=\scriptsize, frame=single]{latex}{Sources/syn-ack-five.tex}
        \end{column}
        \begin{column}{0.36\textwidth}
            \vspace{0.5cm}
            \hspace*{-0.25cm}\input{Sources/syn-ack-five.tex}
        \end{column}
    \end{columns}}
    \end{overprint}
\end{frame}

\begin{frame}[fragile]
    \frametitle{Plotting with \LaTeX (using \TikZ) - random coordinates}
    \begin{overprint}
    \only<1>{\begin{columns}[c]
        \begin{column}{0.62\textwidth}
            \hspace*{-1cm}\inputminted[fontsize=\scriptsize, frame=single]{latex}{Sources/rand-one.tex}
        \end{column}
        \begin{column}{0.38\textwidth}
            \vspace{0.5cm}
            \hspace*{-0.25cm}\input{Sources/rand-one.tex}
        \end{column}
    \end{columns}}
    \only<2>{\begin{columns}[c]
        \begin{column}{0.62\textwidth}
            \hspace*{-1cm}\inputminted[fontsize=\scriptsize, frame=single]{latex}{Sources/rand-two.tex}
        \end{column}
        \begin{column}{0.38\textwidth}
            \vspace{0.5cm}
            \hspace*{-0.25cm}\input{Sources/rand-two.tex}
        \end{column}
    \end{columns}}
    \end{overprint}
\end{frame}

\begin{frame}[fragile]
    \frametitle{Plotting with \LaTeX (using \TikZ) - intersections}
    \begin{overprint}
    \only<1>{\begin{columns}[c]
        \begin{column}{0.62\textwidth}
            \hspace*{-1cm}\inputminted[fontsize=\scriptsize, frame=single]{latex}{Sources/inters-one.tex}
        \end{column}
        \begin{column}{0.38\textwidth}
            \vspace{0.5cm}
            \hspace*{-0.25cm}\input{Sources/inters-one.tex}
        \end{column}
    \end{columns}}
    \only<2>{\begin{columns}[c]
        \begin{column}{0.62\textwidth}
            \hspace*{-1cm}\inputminted[fontsize=\scriptsize, frame=single]{latex}{Sources/inters-two.tex}
        \end{column}
        \begin{column}{0.38\textwidth}
            \vspace{0.5cm}
            \hspace*{-0.25cm}\input{Sources/inters-two.tex}
        \end{column}
    \end{columns}}
    \end{overprint}
\end{frame}

\begin{frame}
    \frametitle{And now?}
    \begin{itemize}
        \item If you liked what you saw, \textcolor{red}{use \TikZ}
        \item Can I draw \texttt{\$(crazy stuff)}? \textcolor{red}{Very likely}
        \item How?
    \end{itemize}
    \pause
    \begin{center}
        \includegraphics[width=0.7\textwidth]{read-the-source.jpg}
    \end{center}
    \vspace{-0.5cm}
    \begin{itemize}
        \item \textcolor{red}{\url{http://www.texample.net/tikz/}}
    \end{itemize}
\end{frame}

\begin{frame}
    \begin{center}
        {\Huge Thanks a lot \\ [1.5cm]}
        \textcolor{red}{If you liked it, and you want the code: \\ [0.5cm]}
    \end{center}
    \hspace*{-0.2cm}\texttt{\footnotesize \mbox{\url{https://alexcriss@bitbucket.org/alexcriss/plotting-in-latex}}}
\end{frame}
\end{document}

